﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicElement : MonoBehaviour
{
    //public GameObject wimController;
    private WorldInMiniature wim;
    private Transform miniMe;
    [SerializeField]
    private float scaleModifier = 1f;
    public float ScaleModifier { set { scaleModifier = value; } }

    void Start()
    {
        LoadMyself();
    }

    void LoadMyself()
    {
        wim = GameObject.FindGameObjectWithTag("WIM").GetComponent<WorldInMiniature>();
        //wt = wimController.GetComponent<WIMTest>();
        if (wim != null)
        {
            //Debug.Log($"{transform.name}: found WolrdInMiniature. Registering myself...");
            miniMe = wim.AddToWIM(gameObject);

        }
        else
        {
            Debug.Log("There was not WIM tag in this scene");
        }
        Debug.Log($"DynElem {gameObject.name} created!");
    }
    private void Update()
    {
        if (wim == null) LoadMyself();

        float scale = wim.ScaleFactor;
        miniMe.localPosition = wim.transform.localPosition + (transform.position * scale); ;
        miniMe.transform.localScale = transform.localScale * (scale * scaleModifier);
        miniMe.localRotation = transform.localRotation;

    }

    public GameObject GetMiniMe()
    {
        if (wim == null) LoadMyself();
        //Debug.Log($"{transform.name}: attempting to get MiniMe");
        return miniMe.gameObject;
    }

}
