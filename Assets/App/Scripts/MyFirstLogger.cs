﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Assertions;

public class MyFirstLogger
{
    //private static MyFirstLogger logger_instance = null;

    //private readonly string LOG_DIRECTORY_PATH = "Assets/App/Logs/";
    //private readonly string LOG_DIRECTORY_PATH = "Assets/App/Logs/";
    private readonly string LOG_DIRECTORY_PATH = @"c:\temp\logs\";
    private string instanceLogPath;
    private readonly string FILE_DATE_PATTERN = "yyyyMMdd_Hmmss";
    private readonly string LOG_DATA_PATTERN = "yyyyMMddHmmss";
    private int logTrackingIdCounter = 1;
    private int logEventIdCounter = 1;
    private string instanceLogDate;
    private static MyFirstLogger instance = null;


    public enum LogType
    {
        TRACKING,
        EVENT,
        DEBUG
    }

    [Serializable]
    public struct LogContentStruct
    {
        public string class_name;
        public string message;
        public int instance_id;
        public Vector3[] tracking_data;
    }

    public MyFirstLogger()
    {
        instanceLogDate = DateTime.Now.ToString(LOG_DATA_PATTERN);
        instanceLogPath = LOG_DIRECTORY_PATH + $"experiment_{instanceLogDate}_logs\\";
        CreateFolder();
        //string full_path = $"{LOG_DIRECTORY_PATH + DateTime.Now.ToString(FILE_DATE_PATTERN)}_experiment.log";
    }

    private void AppendLog(string message)
    {
        //Debug.Log(message);
        string fullPath = $"{instanceLogPath}Log_Tracking_{instanceLogDate}_experiment.json";
        using (StreamWriter writer = File.AppendText(fullPath))
        {
            writer.WriteLine(message);
        }
    }

    public void AppendCSVResults(Trial trial)
    {
        string file = instanceLogPath + $"results_participant{trial.ParticipantID}_{instanceLogDate}.csv";
        string row = trial.GetCSVString();
        if ( !File.Exists(file))
        {
            row = Trial.GetHeader() + "\n" + row;
        }
        using (StreamWriter writer = File.AppendText(file))
        {
            //writer.AutoFlush = true;
            writer.WriteLine(row);
            //writer.Close();
        }
    }
    public void AppendResults(string message, int participantID)
    {
        using (StreamWriter writer = File.AppendText(instanceLogPath+$"results_participant{participantID}_{instanceLogDate}.json"))
        {
            //writer.AutoFlush = true;
            writer.WriteLine(message);
            //writer.Close();
        }
    }

    public void AppendEvent(string message)
    {
        string fullPath = $"{instanceLogPath}Log_Events_{instanceLogDate}_experiment.json";
        using (StreamWriter writer = File.AppendText(fullPath))
        {
            writer.WriteLine(message);
        }
    }

    private void CreateFolder()
    {
        if (Directory.Exists(instanceLogPath))
        {
            Debug.Log($"Folder already exists at: {instanceLogPath}");
        } else
        {
            Debug.Log($"Attempting to create log folder at: {instanceLogPath}");
            System.IO.Directory.CreateDirectory(instanceLogPath);
        }
        Assert.IsTrue(Directory.Exists(instanceLogPath), "Unable to find or create Log folder");
    }

    public void LogEvent(LogContentStruct data)
    {
        JsonTracking jsonObject = new JsonTracking
        {
            log_id = logTrackingIdCounter,
            date = DateTime.Now.ToString(LOG_DATA_PATTERN),
            source_class = data.class_name
        };

        JsonTracking.LogData _ld = new JsonTracking.LogData
        {
            log_type = "Event",
            message = data.message
        };
        //_ld.tracking_data =;
        jsonObject.log_data = _ld;

        string outJson = JsonUtility.ToJson(jsonObject);
        Debug.Log(outJson);
        logTrackingIdCounter++;
        AppendLog(outJson);
    }

    public void LogTracking(LogContentStruct data)
    {
        //Debug.Log("Staring to build JSON");
        JsonTracking jsonObject = new JsonTracking
        {
            log_id = logTrackingIdCounter,
            date = DateTime.Now.ToString(LOG_DATA_PATTERN),
            source_class = data.class_name
        };
        JsonTracking.TrackingData _td= new JsonTracking.TrackingData();
        _td.object_id = data.instance_id;
        _td.position = data.tracking_data[0];
        _td.rotation = data.tracking_data[1];
        _td.forward_direction = data.tracking_data[2];
        JsonTracking.LogData _ld= new JsonTracking.LogData();
        _ld.log_type = "Tracking";
        _ld.message = data.message;
        _ld.tracking_data = _td;
        jsonObject.log_data = _ld;

        string outJson = JsonUtility.ToJson(jsonObject);
        //Debug.Log(outJson);
        logTrackingIdCounter++;
        AppendLog(outJson);
    }

}
