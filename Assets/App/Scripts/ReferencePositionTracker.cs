﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReferencePositionTracker : MonoBehaviour
{
    private DynamicElement dynamicElement;
    void Start()
    {
        dynamicElement = gameObject.AddComponent(typeof(DynamicElement)) as DynamicElement;
        dynamicElement.ScaleModifier = 15f;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newPosition = Camera.main.transform.position;
        newPosition.y = 0.6f;
        transform.position = newPosition;
    }
}
