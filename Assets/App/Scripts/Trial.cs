﻿
using System;
using UnityEngine;

[Serializable]
public class Trial
{
    public enum IndependentVariableValue
    {
        None = 0,
        WorldInMiniature = 1,
        Camera2DPlane = 2,
        Both = 3
    }

    public enum TrialStates
    {
        AIMING, IN_AIR, TARGET_HIT, FINISHED, DEMO
    }

    public enum DistanceValue
    {
        Medium = 0,
        Far = 1
    }
    public enum HeightValue
    {
        BelowEyeLevel = 0,
        EyeLevel = 1,
        AboveEyeLevel = 2
    }

    [Serializable]
    public struct Shot
    {
        public float aiming_time;
        public float closest_distance;
    }

    public int ParticipantID;
    public int TrialID;
    public int Block1;
    public int Block2;
    public IndependentVariableValue IndependentVariable;
    public HeightValue Height;
    public DistanceValue Distance;
    public float total_trial_time;
    public bool target_hit;
    public Shot[] Shots;

    private Shot BestShot()
    {
        Shot closest = Shots[0];
        for (int i = 1; i < Shots.Length; i++)
        {
            if (Shots[i].closest_distance < closest.closest_distance)
                closest = Shots[i];
        }
        return closest;
    }

    private float GetAvgAimingTime()
    {
        float sum = 0;
        foreach (var shot in Shots)
        {
            sum += shot.aiming_time;
        }
        return sum / Shots.Length;
    }

    public string GetCSVString()
    {
        Shot bestShot = BestShot();
        string[] csvRow = new string[] {
            ParticipantID.ToString(),
            TrialID.ToString(),
            Block1.ToString(),
            Block2.ToString(),
            IndependentVariable.ToString(),
            Height.ToString(),
            Distance.ToString(),
            total_trial_time.ToString(),
            Shots.Length.ToString(),
            GetAvgAimingTime().ToString(),
            bestShot.closest_distance.ToString() };
        return string.Join(",", csvRow);
    }

    public static string GetHeader()
    {
        string [] header = new string[]
        {
            "ParticipantID",
            "TrialID",
            "Block1",
            "Block2",
            "IndependentVariable",
            "Height",
            "Distance",
            "TotalTrialTime",
            "ShotsTaken",   
            "AvgAimTime",
            "ClosestDistance"
        };
        return string.Join(",", header);
    }

    public string GetJsonString()
    {
        string json = JsonUtility.ToJson(this);
        return json;
    }

}
