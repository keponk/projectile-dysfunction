﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using Unity;
using UnityEngine;
using UnityEngine.Assertions;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _description;
    [SerializeField]
    private int PARTICIPANT_NUMBER = 1;
    [SerializeField]
    private bool DEMO_ENABLED = false;
    [SerializeField]
    private AudioSource asExperimentBeginSound;
    [SerializeField]
    private AudioSource asExperimentFinishedSound;
    [SerializeField]
    private AudioSource asTargetHitSound;
    private Launcher launcher;
    public GameObject WIM;
    public GameObject CAMERA_2D;
    public GameObject TARGET;
    private bool _launcherFound = false;
    private Trial[] trialsArray;
    private readonly char CSV_DELIMITER = ',';
    private string CSV_FILE_PATH;
    private int currentTrialIndex;
    private Trial.TrialStates currentTrailState;
    private Transform _targetTransform;
    private float _timeBetweenLogs = 0f;
    private readonly float LOG_WAIT_SECS = 0.2f;
    private int _shotCounter = 0;
    private readonly int SHOT_CHANCES = 5;

    private readonly float DISTANCE_MEDIUM = 40f;
    private readonly float DISTANCE_FAR = 85f;
    private readonly float HEIGHT_BELOW_EYE_LEVEL = -8.5f;
    private readonly float HEIGHT_EYE_LEVEL = 1.5f;
    private readonly float HEIGHT_ABOVE_EYE_LEVEL = 12f;

    private MyFirstLogger mylogger;

    private readonly float _betweenTrialWaitTime = 3f;
    private float total_trial_time = 0f;
    private float aiming_time = 0f;
    private Trial.Shot currentShot = new Trial.Shot();
    private List<Trial.Shot> trialShotsList = new List<Trial.Shot>();

    void Start()
    {
        mylogger = new MyFirstLogger();
        //Log.Debug("Logging with Log4Net");
        //Log.Info("Current position = XXXX");
        //Log.Warn("This is a warning");
        //CSV_FILE_PATH = $"Assets/App/Resources/Participant{PARTICIPANT_NUMBER}.csv";
        CSV_FILE_PATH = $"Assets/App/Resources/ProjectileDys.csv";
        FindLauncher();

        LoadTrialsFromCSV();
        if (PARTICIPANT_NUMBER == 420)
        {
            _description.text = "Welcome to the training session.";
        }
        
        //trials = new CSV().GetTrialsFromCSV(logger);
        Assert.IsNotNull(trialsArray);
        //Debug.Log("Trial CSV should be loaded");
        //Debug.Log($"Trials[0].ID={trialsArray[0].TrialID}");

        // SETUP TARGET AND HIDE.
        GameObject trialTarget = Instantiate(TARGET, new Vector3(0, 10, 0), Quaternion.identity, transform);
        //trialTarget.SetActive(false);
        _targetTransform = trialTarget.transform;
        _targetTransform.localScale = Vector3.zero;
        currentTrialIndex = 0;
        CAMERA_2D.transform.localScale = new Vector3(0, 0, 0);
        WIM.transform.localScale = new Vector3(0, 0, 0);
        StartCoroutine(StartWaitTime());
        
    }
    
    private void FindLauncher()
    {
        GameObject launcherGO = GameObject.FindGameObjectWithTag("Launcher");
        if (launcherGO == null)
        {
            Debug.Log("No launcher object found. Possible due to no controller.");
            _launcherFound = false;
        }
        else
        {
            launcher = launcherGO.GetComponent<Launcher>();
            _launcherFound = true;
        }
    }

    public void LogEvent(MyFirstLogger.LogContentStruct payload)
    {
        mylogger.LogEvent(payload);
    }

    public void LogTracking(MyFirstLogger.LogContentStruct payload)
    {
        //Debug.Log($"Recieved payload in LogTrcking!, instanceid={payload.instance_id}");
        mylogger.LogTracking(payload);
    }

    public void TargetEventHandler(bool hit, float distance)
    {
        //Log.Info( "NEED TO FIX THIS MESSAGE. Target was hit.");

        // LOG COLLISION TIME
        currentShot.closest_distance = distance;
        currentShot.aiming_time = aiming_time;
        trialShotsList.Add(currentShot);
        currentShot = new Trial.Shot();
        if (hit || _shotCounter >= SHOT_CHANCES)
        {
            if (hit) asTargetHitSound.Play();
            //_targetTransform.gameObject.SetActive(false);
            _targetTransform.localScale = Vector3.zero;
            _targetTransform.gameObject.GetComponent<DynamicElement>().GetMiniMe().SetActive(false);
            trialsArray[currentTrialIndex].target_hit = hit;
            trialsArray[currentTrialIndex].total_trial_time = total_trial_time;
            trialsArray[currentTrialIndex].Shots = new Trial.Shot[trialShotsList.Count];
            currentTrailState = Trial.TrialStates.FINISHED;
            for (int i = 0; i < trialShotsList.Count; i++)
            {
                trialsArray[currentTrialIndex].Shots[i] = trialShotsList[i];
            }

            trialShotsList = new List<Trial.Shot>();
            RecordResults();
            currentTrialIndex++;
            if (currentTrialIndex >= trialsArray.Length)
            {
                asExperimentFinishedSound.Play();
                Debug.Log("Experiment finished for this participant");
                ShowGameOverMessage();
                Application.Quit();
            } else
            {
                _description.text = "Wait 5 seconds for next target.";
                StartCoroutine(WaitForNexTrial());
                
            }
        } else
        {
            currentTrailState = Trial.TrialStates.AIMING;
            aiming_time = 0f;
        }
        //_description.text = $"Trail # { trialsArray[currentTrialIndex].TrialID.ToString()}.\nYou have {SHOT_CHANCES - _shotCounter} shots.";
    }

    private void RecordResults()
    {
        Debug.Log($"ResultsJson = {trialsArray[currentTrialIndex].GetJsonString()}");
        //for (int i = 0; i < trialsArray[currentTrialIndex].Shots.Length; i++)
        //{
        //    string new_line = $"";
        //}
        mylogger.AppendResults(trialsArray[currentTrialIndex].GetJsonString(), PARTICIPANT_NUMBER);
        mylogger.AppendCSVResults(trialsArray[currentTrialIndex]);

    }

    private void ShowGameOverMessage()
    {
        _description.text = "Good Job! We are done.";
    }

    private void NextTrial()
    {
        Debug.Log(trialsArray[currentTrialIndex].GetJsonString());
        Trial current = trialsArray[currentTrialIndex];
        
        Debug.Log($"Starting Trial #{current.TrialID}");
        //Log.Debug($"DependentVariale={current.DependentVariable.ToString()}");
        _shotCounter = 0;
        _description.text = $"Trial # { current.TrialID.ToString()}.\nYou have {SHOT_CHANCES-_shotCounter} shots.";
        switch (current.IndependentVariable)
        {
            case Trial.IndependentVariableValue.None:
                Debug.Log($"NoWidget");
                WIM.transform.localScale = new Vector3(0, 0, 0);
                //WIM.GetComponent<Renderer>().enabled = false;
                //CAMERA_2D.GetComponent<Renderer>().enabled = false;
                CAMERA_2D.transform.localScale = new Vector3(0, 0, 0);
                break;
            case Trial.IndependentVariableValue.WorldInMiniature:
                Debug.Log($"WorldInMiniature");
                WIM.transform.localScale = new Vector3(1, 1, 1);
                //WIM.GetComponent<Renderer>().enabled = true;
                //CAMERA_2D.GetComponent<Renderer>().enabled = false;
                CAMERA_2D.transform.localScale = new Vector3(0, 0, 0);
                break;
            case Trial.IndependentVariableValue.Camera2DPlane:
                Debug.Log($"Camera_2D");
                WIM.transform.localScale = new Vector3(0, 0, 0);
                //WIM.GetComponent<Renderer>().enabled = false;
                //CAMERA_2D.GetComponent<Renderer>().enabled = true;
                CAMERA_2D.transform.localScale = new Vector3(1,1,1);
                break;
        }
        Vector3 targetPosition = Vector3.zero;
        switch (current.Height)
        {
            case Trial.HeightValue.BelowEyeLevel:
                Debug.Log($"Calculate Beloweye position");
                targetPosition.y = HEIGHT_BELOW_EYE_LEVEL;
                break;
            case Trial.HeightValue.EyeLevel:
                Debug.Log($"Calculate EyeLevel position");
                targetPosition.y = HEIGHT_EYE_LEVEL;
                break;
            case Trial.HeightValue.AboveEyeLevel:
                Debug.Log($"Calculate AboveEyeLevel position");
                targetPosition.y = HEIGHT_ABOVE_EYE_LEVEL;
                break;
        }

        switch (current.Distance)
        {
            case Trial.DistanceValue.Medium:
                Debug.Log($"Calculate Medium distance position");
                targetPosition.z = DISTANCE_MEDIUM;
                break;
            case Trial.DistanceValue.Far:
                Debug.Log($"Calculate Far distance position");
                targetPosition.z = DISTANCE_FAR;
                break;
        }

        // log stuff
        MyFirstLogger.LogContentStruct payload = new MyFirstLogger.LogContentStruct();
        payload.class_name = "GameController";
        payload.instance_id = GetInstanceID();
        payload.message = $"Trial #{current.TrialID} loaded";
        LogEvent(payload);
        
        // Place target 
        Debug.Log($"Traget created at {targetPosition}");
        _targetTransform.position = targetPosition;
        //_targetTransform.gameObject.SetActive(true);
        _targetTransform.localScale = Vector3.one;
        _targetTransform.gameObject.GetComponent<DynamicElement>().GetMiniMe().SetActive(true);

        // Gather time info
        //Trial.Shot new_shot = new Trial.Shot();
        currentTrailState = Trial.TrialStates.AIMING;
        current.target_hit = false;
        aiming_time = 0f;
        total_trial_time = 0f;
        //trialShotsList
    }
    IEnumerator WaitForNexTrial()
    {
        //This is a coroutine
        Debug.Log("Started corouting, waiting for next trial.");
        yield return new WaitForSeconds(_betweenTrialWaitTime);  //Wait
        //Debug.Log("End Wait() function and the time is: " + Time.time);
        NextTrial();
    }

    IEnumerator StartWaitTime()
    {
        int beginWaitTime = 10;
        int currCountdownValue = beginWaitTime + (int)asExperimentBeginSound.clip.length;
        while (currCountdownValue > 0)
        {
            _description.text = $"Experiment is about to start in {currCountdownValue}...";
            if ((int)asExperimentBeginSound.clip.length == currCountdownValue) asExperimentBeginSound.Play();
            yield return new WaitForSeconds(1.0f);
            currCountdownValue--;
        }
        NextTrial();
    }

    public Trial.TrialStates GetCurrentTrailStatus()
    {
        return currentTrailState;
    }

    public void UpdateCurrentTrialStatus(Trial.TrialStates new_status)
    {
        _shotCounter++;
        _description.text = $"Trial # { trialsArray[currentTrialIndex].TrialID.ToString()}.\nYou have {SHOT_CHANCES-_shotCounter} shots.";
        currentTrailState = new_status;
    }

    private void LoadTrialsFromCSV()
    {
        Debug.Log("Loading CSV file.");
        Assert.IsTrue(System.IO.File.Exists(CSV_FILE_PATH), "File doesn't exists");
        StreamReader inp_stm = new StreamReader(CSV_FILE_PATH);
        string[] rows = inp_stm.ReadToEnd().Split('\n');
        List<Trial> tmp_trials = new List<Trial>();
        //Debug.Log("fields: " + fields);
        Debug.Log("About to enter for loop");
        for (int i = 1; i < rows.Length; i++)
        {
            string[] fields = rows[i].Split(CSV_DELIMITER);
            if (int.Parse(fields[1]) != PARTICIPANT_NUMBER)  continue;
            //Debug.Log($"Parsig row #{i}: {rows[i]}, for participant {PARTICIPANT_NUMBER}");
            Trial trial = new Trial
            {
                // Ignore first fiels, its the name of the designname
                ParticipantID = int.Parse(fields[1]),
                TrialID = int.Parse(fields[2]),
                Block1 = int.Parse(fields[3]),
                Block2 = int.Parse(fields[4]),
                IndependentVariable = (Trial.IndependentVariableValue) Enum.Parse(typeof(Trial.IndependentVariableValue), fields[5]),
                Height = (Trial.HeightValue)Enum.Parse(typeof(Trial.HeightValue), fields[6]),
                Distance = (Trial.DistanceValue)Enum.Parse(typeof(Trial.DistanceValue), fields[7])
            };            
            tmp_trials.Add(trial);
        }
        //trialsArray = new Trial[tmp_trials.Count];
        trialsArray = tmp_trials.ToArray();
        Assert.IsTrue(trialsArray.Length > 1, "trials array is not big enough");
        Debug.Log(trialsArray[0].GetJsonString());
        Debug.Log("All trials loaded");
        inp_stm.Close();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!_launcherFound)
        {
            FindLauncher();
        }
        if (_timeBetweenLogs >= LOG_WAIT_SECS)
        {
            Vector3[] tracked_data = { Camera.main.transform.position, Camera.main.transform.eulerAngles, Camera.main.transform.forward };
            MyFirstLogger.LogContentStruct payload = new MyFirstLogger.LogContentStruct
            {
                class_name = typeof(GameController).Name,
                instance_id = GetInstanceID(),
                message = "Camera tracking data",
                tracking_data = tracked_data
            };
            //Debug.Log($"in fixed update, instanceId={payload.instance_id}");
            LogTracking(payload);

            //Log.Info($"\"object_id\":\"camera\", \"position\":\"{Camera.main.transform.position}\", \"rotation\":\"{Camera.main.transform.eulerAngles}\", \"forward\":\"{Camera.main.transform.forward}\"");
            _timeBetweenLogs = 0f;
        }
        _timeBetweenLogs += Time.deltaTime;

        // results
        total_trial_time += Time.deltaTime;
        aiming_time += Time.deltaTime;
    }

}
