﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldInMiniature : MonoBehaviour
{
    [SerializeField]
    private float _scaleFactor = 0.01f;
    public float ScaleFactor { get => _scaleFactor; }

    void Awake()
    {
        Vector3 minimapCenter = transform.position;
        minimapCenter.y += 0.5f;


        GameObject g_environment = GameObject.FindWithTag("Environment");
        GameObject worldInMiniature = Instantiate(g_environment, transform);
        worldInMiniature.name = "WordInMinuatureClone";
        var allComponents = worldInMiniature.GetComponentsInChildren<Component>();
        foreach (Component c in allComponents)
        {
            if (c is MeshCollider || c is Rigidbody)
            {
                Destroy(c);
            }
        }
        worldInMiniature.transform.localScale *= _scaleFactor;
        worldInMiniature.transform.position = transform.position;
    }

    public Transform AddToWIM(GameObject t)
    {
        //Debug.Log("Adding new WIM element. Scale is : " + _scaleFactor);
        Vector3 temppos = t.transform.position * _scaleFactor;
        //Debug.Log($"New expected deltaPos is: WIM + {temppos}");
        Vector3 miniSpanwnPosition = transform.position + (t.transform.position * _scaleFactor);
        //Debug.Log($"New expected final position is: WIM + {miniSpanwnPosition}");
        GameObject newMiniObject = Instantiate(t.gameObject, miniSpanwnPosition, t.transform.rotation, transform);
        newMiniObject.transform.localScale *= _scaleFactor;
        newMiniObject.name = "Mini" + t.name;
        //Debug.Log($"Object: {t.name} ;  scale: {t.transform.localScale} ; newObjScale: {go.transform.localScale}");
        var allComponents = newMiniObject.GetComponentsInChildren<Component>();
        foreach (Component c in allComponents)
        {
            //Debug.Log("Found TrailRenderer!");
            if (c is TrailRenderer _tr)
            {
                _tr.startWidth *= _scaleFactor;
            }
            //Debug.Log($"Object {c.gameObject.name} ;  component {c.GetType().Name}");
            if (!(c is MeshFilter || c is Renderer || c is Transform || c is Animator))
            {
                Destroy(c);
            }

        }
        if (newMiniObject.tag == "Player")
        {
            Debug.Log("Player found in WIMTests");
            //t.GetComponent<MeshRenderer>().enabled = false;
        }
        return newMiniObject.transform;
    }
}
