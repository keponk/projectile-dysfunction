﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Assertions;

public class CSV 
{
    private string FILE_PATH = "Assets/App/Resources/Test.csv";
    //private List<Trial> CSV;
    private short trialLength = 0;
    private Trial[] trial_array;
    private MyFirstLogger _logger;

    private string[] LoadFile()
    {
        TextAsset file_data = Resources.Load<TextAsset>("ProjectileDys.csv");
        Assert.IsTrue(file_data.bytes.Length > 1);
        //_logger.LogEvent("File loaded");
        return file_data.text.Split(new char[] { '\n' });
    }

}
