﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.XR;

[RequireComponent(typeof(AudioSource))]
public class Launcher : MonoBehaviour
{
    public GameObject trailDotPrefab;
    public Transform AMMO;
    public float LAUNCH_FORCE = 20f;
    public float PROJECTILE_MASS = 1f;

    private readonly float DOT_TIME_STEP = 0.1f;
    private readonly float MAGNITUDE_CHANGE_STEP = 2f;
    private readonly Vector3 GRAVITY = Physics.gravity;
    private readonly int TRAJECTORY_MAX_SIZE = 50;
    private readonly float OFFSET_FROM_GUN = 0.28f;
    private readonly float GROUND_LEVEL = -10f;

    private float TRAJECTORY_OFFSET = 0.008f;
    private float TRAJECTORY_OFFSET_STEP = 0.0008f;
    private const float TRAJECTORY_OFFSET_LIMIT = 0.1f;
    private const float FORMULA_CONSTANT = 0.5f;
    private Vector3 INITIAL_POSITION = Vector3.zero;
    private Vector3 LAUNCH_VELOCITY = Vector3.forward;
    private GameObject[] trajectory;
    private readonly float COOLDOWN = 10f;
    private float timeSinceLastShot = 10f;
    private List<Projectile> currentProjectiles;

    public AudioSource audioShoot;
    public AudioSource audioPowerUp;
    public AudioSource audioPowerDown;

    private float _timeBetweenLogs = 0f;
    private readonly float LOG_WAIT_SECS = 0.2f;
    GameController gameControllerReference;

    //private static readonly ILog Log = LogManager.GetLogger(typeof(Launcher));

    void Start()
    {
        GenerateDots();
        gameControllerReference = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    void Update()
    {
        //CleanDuplicates();

        if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
        {
            IncreasePower();
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
        {
            DecreasePower();
        }

        if (timeSinceLastShot < COOLDOWN)
        {
            timeSinceLastShot += Time.deltaTime;
        }
        else
        {

        }

        ResetTrajectory();
        DrawTrajectory();
        //if (_isTrajectoryEnabled) DrawTrajectory();
    }

    private void CleanDuplicates()
    {
        GameObject launcherObject = GameObject.FindGameObjectWithTag("Launcher");
        if (launcherObject.GetComponent<Launcher>() != this)
            Destroy(this);
    }

    void FixedUpdate()
    {
        if (_timeBetweenLogs >= LOG_WAIT_SECS)
        {
            //Log.Info($"\"object_id\":\"{ID}\",\"position\":\"{transform.position}\",\"velocity\":\"{transform.GetComponent<Rigidbody>().velocity}\"");
            Vector3[] tracked_data = { transform.position, transform.eulerAngles, transform.forward };
            MyFirstLogger.LogContentStruct payload = new MyFirstLogger.LogContentStruct
            {
                class_name = typeof(Launcher).Name,
                instance_id = GetInstanceID(),
                message = "Tracking Launcher data",
                tracking_data = tracked_data
            };
            //Debug.Log($"in fixed update, instanceId={payload.instance_id}");
            gameControllerReference.LogTracking(payload);
            _timeBetweenLogs = 0f;
        }
        _timeBetweenLogs += Time.deltaTime;
    }

    private void DrawTrajectory()
    {
        bool isYBelowZero = false;
        int elemsIndex = 0;
        LAUNCH_VELOCITY = transform.forward * (LAUNCH_FORCE / PROJECTILE_MASS);
        INITIAL_POSITION = transform.position;
        INITIAL_POSITION += (transform.forward * OFFSET_FROM_GUN);
        while (!isYBelowZero && elemsIndex < TRAJECTORY_MAX_SIZE)
        {
            trajectory[elemsIndex].transform.position = CalculatePosition(DOT_TIME_STEP * (elemsIndex) + TRAJECTORY_OFFSET);
            trajectory[elemsIndex].GetComponent<Renderer>().enabled = true;
            trajectory[elemsIndex].GetComponent<DynamicElement>().GetMiniMe().GetComponent<Renderer>().enabled = true;
            if (trajectory[elemsIndex].transform.position.y < GROUND_LEVEL - 5) isYBelowZero = true;
            elemsIndex++;
        }
        TRAJECTORY_OFFSET += TRAJECTORY_OFFSET_STEP;
        //Debug.Log($"TRAJECTORY_OFFSET : {TRAJECTORY_OFFSET}");
        if (TRAJECTORY_OFFSET >= TRAJECTORY_OFFSET_LIMIT) TRAJECTORY_OFFSET = 0.01f;
    }

    public void Shoot()
    {
        Trial.TrialStates currentState = gameControllerReference.GetCurrentTrailStatus();
        Debug.Log($"Attempting to shoo with status: {currentState.ToString()}");
        if (currentState == Trial.TrialStates.AIMING || currentState == Trial.TrialStates.DEMO)
        {
            Transform projectile = Instantiate(AMMO, INITIAL_POSITION, transform.rotation);
            LAUNCH_VELOCITY = transform.forward * (LAUNCH_FORCE / PROJECTILE_MASS);
            //Debug.Log($"Launch Vel = {LAUNCH_VELOCITY}");
            projectile.GetComponent<Rigidbody>().mass = PROJECTILE_MASS;
            projectile.GetComponent<Rigidbody>().AddForce(LAUNCH_VELOCITY, ForceMode.Impulse);
            //Debug.Log($"\"id\":\"{GetInstanceID()}\", Projectile shot with LAUNCHED_VELOCITY={LAUNCH_VELOCITY}.\"");
            timeSinceLastShot = 0f;
            audioShoot.Play();
            MyFirstLogger.LogContentStruct payload = new MyFirstLogger.LogContentStruct
            {
                class_name = "Launcher",
                instance_id = GetInstanceID(),
                message = "Shot fired."
            };
            gameControllerReference.UpdateCurrentTrialStatus(Trial.TrialStates.IN_AIR);
            gameControllerReference.LogEvent(payload);
        }
        else
        {
            Debug.Log($"\"id\":\"{GetInstanceID()}\", Not enough time since last shot.");
        }
    }

    private Vector3 CalculatePosition(float elapsedTime)
    {
        return GRAVITY * elapsedTime * elapsedTime * FORMULA_CONSTANT + LAUNCH_VELOCITY * elapsedTime + INITIAL_POSITION;
    }

    private void ResetTrajectory()
    {
        foreach (GameObject go in trajectory)
        {
            go.GetComponent<Renderer>().enabled = false;
            go.GetComponent<DynamicElement>().GetMiniMe().GetComponent<Renderer>().enabled = false;
        }
    }

    private void GenerateDots()
    {
        GameObject trajectoryArray = new GameObject
        {
            name = "TrajectoryPool"
        };
        trajectoryArray.transform.parent = transform;
        trajectory = new GameObject[TRAJECTORY_MAX_SIZE];
        for (int i = 0; i < trajectory.Length; i++)
        {
            //Debug.Log($"Creating single dot at index {i}");
            GameObject trajectoryDot = (GameObject)Instantiate(trailDotPrefab);
            trajectoryDot.name = "TrajectoryDotClone" + i;
            trajectoryDot.GetComponent<Renderer>().enabled = false;
            trajectoryDot.transform.parent = trajectoryArray.transform;
            trajectoryDot.GetComponent<Rigidbody>().useGravity = false;
            trajectory[i] = trajectoryDot;

            //trajectoryDot.GetComponent<DynamicElement>().GetMiniMe().GetComponent<Renderer>().enabled = false;
            //DynamicElement dynEl = new DynamicElement();
            //Debug.Log($"creating traj dynElem index : {i}");
            //DynamicElement _dynElem = trajectoryDot.AddComponent<DynamicElement>();
            //Assert.IsNotNull(_dynElem);
            //_dynElem.GetMiniMe().GetComponent<Renderer>().enabled = false;
            //_dynElem.ScaleModifier = 1f;
            //Debug.Log("Finished creating single dot");
        }
    }

    public void IncreasePower()
    {
        audioPowerUp.Play();
        LAUNCH_FORCE += MAGNITUDE_CHANGE_STEP;
        MyFirstLogger.LogContentStruct payload = new MyFirstLogger.LogContentStruct();
        payload.class_name = "Launcher";
        payload.instance_id = GetInstanceID();
        if (LAUNCH_FORCE > 100f)
        {
            payload.message = $"Increase LAUNCH_FORCE action; value is already maximum at {LAUNCH_FORCE}";
            LAUNCH_FORCE = 100f;
        }
        else
        {
            //TRAJECTORY_OFFSET_STEP *= 0.01f;
            payload.message = $"Increase LAUNCH_FORCE action; new value is {LAUNCH_FORCE}";
        }
        gameControllerReference.LogEvent(payload);
    }

    public void DecreasePower()
    {
        audioPowerDown.Play();
        MyFirstLogger.LogContentStruct payload = new MyFirstLogger.LogContentStruct();
        payload.class_name = "Launcher";
        payload.instance_id = GetInstanceID();
        LAUNCH_FORCE -= MAGNITUDE_CHANGE_STEP;
        if (LAUNCH_FORCE < 1f)
        {
            LAUNCH_FORCE = 1f;
            payload.message = $"Decrease LAUNCH_FORCE action; value is already minimum at {LAUNCH_FORCE}";
        }
        else
        {
            //TRAJECTORY_OFFSET_STEP *= 0.05f;
            payload.message = $"Decrease LAUNCH_FORCE action; new value is {LAUNCH_FORCE}";
        }
        gameControllerReference.LogEvent(payload);
    }

}
