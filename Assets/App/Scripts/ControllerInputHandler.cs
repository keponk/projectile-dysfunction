﻿using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Physics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ControllerInputHandler : MonoBehaviour, IMixedRealityInputHandler, IMixedRealityInputHandler<Vector2>
{
    //public MixedRealityInputAction _touchpadClick = MixedRealityInputAction.None;
    //public MixedRealityInputAction _touchpadPosition = MixedRealityInputAction.None;
    //public MixedRealityInputAction _gripPressed = MixedRealityInputAction.None;

    private bool gripDown = false;
    private Vector2 lastPosition = Vector2.zero;
    private Launcher launcher;
    WindowsMixedRealityControllerVisualizer wmrController;
    private GameObject gunPrefab;

    void Start()
    {
        gunPrefab = transform.gameObject;
        //launcher = gameObject.AddComponent<Launcher>();
        //LoadComponents();
        //gunPrefab = GameObject.FindGameObjectWithTag("Launcher");
        //launcher = gunPrefab.GetComponent<Launcher>();
    }

    void Update()
    {
        if (gunPrefab == null)
        {
            Debug.Log("No gun available. Searching Launcher tag...");
            gunPrefab = GameObject.FindGameObjectWithTag("Launcher");
        }
        if (launcher == null)
        {
            Debug.Log("No launcher script available. Searching component....");
            launcher = gameObject.GetComponent<Launcher>();
        }
    }


    void LoadComponents()
    {
        if (launcher == null)
        {
            launcher = transform.GetComponent<Launcher>();
            //Debug.Log("Launcher not found?!?!?");
        }
        if (wmrController == null)
        {
            wmrController = gunPrefab.GetComponent<WindowsMixedRealityControllerVisualizer>();
        }
    }

    public void OnInputChanged(InputEventData<Vector2> eventData)
    {
        //Debug.Log($"OnInputChanged: actionDescription: {eventData.MixedRealityInputAction.Description} ; inpuData:{eventData.InputData}");
        if (eventData.MixedRealityInputAction.Description == "Touchpad Position")
        {
            lastPosition = eventData.InputData;
            //Debug.Log("Touchpad position:  " + lastPosition);
        }
        //eventData.Use();
    }

    public void OnInputDown(InputEventData eventData)
    {
        //Debug.Log($"OnInputDown: actionDescription: {eventData.MixedRealityInputAction.Description} ; actionID: {eventData.MixedRealityInputAction.Id} ; actionAxis: {eventData.MixedRealityInputAction.AxisConstraint}");

        //Debug.Log($"OnInputDown: {eventData.MixedRealityInputAction.Description}");
        if (eventData.MixedRealityInputAction.Description == "Grip Press")
        {
            //Debug.Log($"Input DOWN: Grip Pressed : Enable Trajectory");
            if (launcher != null)
            {
                //launcher.TrajectoryEnabled = gripDown = true;
                //gripDown = true;
            }


        }
        //eventData.Use();

    }

    public void OnInputUp(InputEventData eventData)
    {
        //Debug.Log($"OnInputUp: actionDescription: {eventData.MixedRealityInputAction.Description} ; actionID: {eventData.MixedRealityInputAction.Id} ; actionAxis: {eventData.MixedRealityInputAction.AxisConstraint}");
        //if (eventData.MixedRealityInputAction.Description == "Grip Press")
        //{
        //    //Debug.Log($"Input UP: Grip Pressed : Disable Trajectory");
        //    if (launcher != null) launcher.TrajectoryEnabled = gripDown = false;
        //    if (launcher != null) gripDown = false;
        //}
        if (eventData.MixedRealityInputAction.Description == "Touchpad Pressed")
        {
            //Debug.Log("Touchpad position:  " + lastPosition);
            if (lastPosition.y > 0)
            {
                //Debug.Log($"INCREASE POWER!");
                if (launcher != null) launcher.IncreasePower();
            }
            else
            {
                //Debug.Log($"DECREASE POWER!");
                if (launcher != null) launcher.DecreasePower();
            }
        }
        if (eventData.MixedRealityInputAction.Description == "Select" && launcher != null)
        {
            launcher.Shoot();
        }
        //eventData.Use();

    }


}
