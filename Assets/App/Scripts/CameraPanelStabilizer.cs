﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPanelStabilizer : MonoBehaviour
{
    [SerializeField]
    private float controlledDistance = 1f;
    [SerializeField]
    private float speed = 0.001f;
    [SerializeField]
    private float errorTolerance = 0.1f;
    private Vector3 origin;
    private float maxDistance;
    private float minDistance;

    void Start()
    {
        origin = transform.position;
        maxDistance = controlledDistance + errorTolerance;
        minDistance = controlledDistance - errorTolerance;
        Debug.Log($"Origin = {origin}");
    }
    void Update()
    {
        float cameraY = Camera.main.transform.position.y;
        //Debug.Log($"cameraPos = {Camera.main.transform.position}");
        Vector3 newPos = transform.position;
        Debug.Log($"panel position = {newPos}");
        float distanceFromCamera = cameraY - newPos.y;
        Debug.Log($"distancefromcamera = {distanceFromCamera}");
        if (distanceFromCamera > maxDistance || distanceFromCamera < minDistance)
        {
            if (distanceFromCamera > controlledDistance)
            {
                //Debug.Log("Move panel up");
                newPos.y += speed;
            }
            else if (distanceFromCamera < controlledDistance)
            {
                //Debug.Log("Move panel down");
                newPos.y -= speed;
            }
            //newPos.x = origin.x;
            //newPos.z = origin.z;
            transform.position = newPos;
        }
    }
}
