﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class JsonTracking
{
    [Serializable]
    public struct TrackingData
    {
        public int object_id;
        public Vector3 position;
        public Vector3 rotation;
        public Vector3 forward_direction;
    }

    [Serializable]
    public struct LogData
    {
        public string log_type;
        public string message;
        public TrackingData tracking_data;
    }

    public int log_id;
    public string date;
    public string source_class;
    public LogData log_data;

}
