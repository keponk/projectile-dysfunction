﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class JsonEvent 
{
    [Serializable]
    public struct LogData
    {
        public string log_type;
        public string message;
    }

    public int log_id;
    public string date;
    public string source_class;
    public LogData log_data;

}
