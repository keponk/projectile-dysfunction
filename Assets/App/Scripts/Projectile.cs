﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private float TIME_TO_LIVE = 8f;

    private float hinit = 0f;
    private float xzero = 0f;
    private float hmax = 0f;
    private float time_to_hmax = 0f;
    private float time_to_floor = 0f;
    private DynamicElement dynamicElement;
    private readonly float LOG_WAIT_SECS = 0.2f;
    private float _timeBetweenLogs = 0f;
    private bool _trackingTarget = false;
    private GameObject _target;
    private float _distanceToTarget = -1;
    private GameController gameControllerReference;
    //private static readonly ILog Log = LogManager.GetLogger(typeof(Projectile));

    //public void DisplayInfo(bool status)
    //{
    //    isVerbose = status;
    //}


    void Start()
    {
        xzero = transform.position.x;
        hinit = transform.position.y;
        hmax = hinit;
        //Debug.Log("Rotation: " + transform.rotation);
        //dynamicElement = gameObject.AddComponent(typeof(DynamicElement)) as DynamicElement;
        dynamicElement = gameObject.AddComponent<DynamicElement>();
        dynamicElement.ScaleModifier = 2f;
        //TrailRenderer _tr = gameObject.GetComponent<TrailRenderer>();
        //Debug.Log("TrailRenderer startWidth = " + _tr.startWidth);
        //Log.Info($"Projectile created with GUID {ID}");

        gameControllerReference = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

        // FIND TARGET
        FindTarget();
    }

    void Update()
    {
        if (TIME_TO_LIVE <= 0f || transform.position.y < -10f)
        {
            //Log.Debug($"id={ID}, Destroying projectile and clone.");
            //Destroy(dynamicElement.GetMiniMe());
            //Destroy(gameObject);
            gameControllerReference.TargetEventHandler(false, _distanceToTarget);
            DestroyMe();
        }
        TIME_TO_LIVE -= Time.deltaTime;
    }

    void FixedUpdate()
    {
        if (_timeBetweenLogs >= LOG_WAIT_SECS)
        {
            //Log.Info($"\"object_id\":\"{ID}\",\"position\":\"{transform.position}\",\"velocity\":\"{transform.GetComponent<Rigidbody>().velocity}\"");
            Vector3[] tracked_data = { transform.position, transform.eulerAngles, transform.forward };
            MyFirstLogger.LogContentStruct payload = new MyFirstLogger.LogContentStruct
            {
                class_name = typeof(Projectile).Name,
                instance_id = GetInstanceID(),
                message = "Tracking the projectile",
                tracking_data = tracked_data
            };
            //Debug.Log($"in fixed update, instanceId={payload.instance_id}");

            if (gameControllerReference != null)
            {
                gameControllerReference.LogTracking(payload);
            }
            else
            {
                Debug.LogWarning("Projectile was unable to find GameController");
            }
            _timeBetweenLogs = 0f;
        }
        _timeBetweenLogs += Time.deltaTime;

        // check distance to target

        float currentDistance = Vector3.Distance(transform.position, _target.transform.position);
        if (currentDistance < _distanceToTarget)
        {
            _distanceToTarget = currentDistance;
        }


    }

    private void FindTarget()
    {
        _target = GameObject.FindGameObjectWithTag("Target");
        _distanceToTarget = Vector3.Distance(transform.position, _target.transform.position);
        _trackingTarget = true;
        //if (_target != null && _target.activeSelf)
        //{
        //} 
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log($"Projectile just hit {collision.gameObject.name}");
        if (_trackingTarget)
        {
            if (collision.gameObject.tag == "Target")
            {
                //collision.gameObject.SetActive(false);
                gameControllerReference.TargetEventHandler(true, _distanceToTarget);
                DestroyMe();
            }
        }
    }

    private void DestroyMe() //to be used in the future
    {
        Destroy(dynamicElement.GetMiniMe());
        Destroy(gameObject);
    }

}
