using System.IO;
using log4net.Config;
using UnityEngine;

public static class LoggingConfiguration
{
    //[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]

    private static void Configure()
    {
        //log4net.GlobalContext.Properties["LogFileName"] = @"E:\\file1";
        XmlConfigurator.Configure(new FileInfo($"{Application.dataPath}/App/Libraries/log4net.xml"));
        
    }
}