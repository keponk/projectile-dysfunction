# Projectile Dysfunction

Prototypyng work for thesis project at LIMSI - VENISE team.

Master student: Joel Gil León

## Vizualization of newtonian physics phenomena in an immersive environment.
Project focuses the different ways of analyzing a projectile trajectory for the purpose of teaching physics concepts.

In this project I create a prototype to be used in a controlled experiment to compare a 2D camera view of the scene and a 3D world-in-miniature object, both inside a Virtual environment using an MS Mixed Reality HMD as hardware. Project Developed using Unity v2018.4 LTS and [Microsoft Mixed Reality Toolkit v2.0.0 RC2.1](https://github.com/microsoft/MixedRealityToolkit-Unity/releases/tag/v2.0.0-RC2.1)

## Instructions

* Import [MRTK foundation](https://github.com/microsoft/MixedRealityToolkit-Unity/releases/download/v2.0.0-RC2.1/Microsoft.MixedReality.Toolkit.Unity.Foundation-v2.0.0-RC2.1.unitypackage) package
* Import [MRTK Examples](https://github.com/microsoft/MixedRealityToolkit-Unity/releases/download/v2.0.0-RC2.1/Microsoft.MixedReality.Toolkit.Unity.Foundation-v2.0.0-RC2.1.unitypackage) 
* Select **PD_MixedRealityToolkitConfigurationProfile** from `Assets\App\MRTKCustomProfiles`

## Tags
Current tags in use:
* WIM
* Environment
* Launcher
* TextPanel
* Player (not referenced)
* MainCamera (not referenced)
* Target 
* GameController